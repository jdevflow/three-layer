package com.example.demo.infrastructure;

import java.util.Optional;

import com.example.demo.domain.User;

public interface UserRepository {

    Optional<User> findByLogin(final String login);

    User create(User user);
}
