package com.example.demo.infrastructure;

import java.util.Optional;

import org.springframework.stereotype.Repository;

import com.example.demo.domain.User;
import com.example.demo.infrastructure.jpa.JpaUserRepository;
import lombok.AllArgsConstructor;

@AllArgsConstructor
@Repository
public class UserRepositoryImpl implements UserRepository {

    private final JpaUserRepository jpaUserRepository;

    @Override
    public Optional<User> findByLogin(String login) {
        Optional<UserDb> userDb = jpaUserRepository.findByLogin(login);
        return userDb.map(UserDb::toUser);
    }

    @Override
    public User create(User user) {
        UserDb userDb = UserDb.from(user);
        jpaUserRepository.save(userDb);
        return user;
    }
}
