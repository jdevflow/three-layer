package com.example.demo.infrastructure.jpa;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.demo.infrastructure.UserDb;

public interface JpaUserRepository extends JpaRepository<UserDb, String> {


    Optional<UserDb> findByLogin(String s);

}
