package com.example.demo.infrastructure;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

import com.example.demo.domain.User;
import lombok.Setter;

@Setter
@Entity
public class UserDb {

    @Id
    private String id;

    @Column(length = 32)
    private String login;
    @Column(length = 64)
    private String password;

    public User toUser() {
        return User.builder()
                .id(id)
                .login(login)
                .password(password)
                .build();
    }

    public static UserDb from(final User user) {
        final UserDb userDb = new UserDb();
        userDb.setId(user.getId());
        userDb.setPassword(user.getPassword());
        userDb.setLogin(user.getLogin());
        return userDb;
    }

}
