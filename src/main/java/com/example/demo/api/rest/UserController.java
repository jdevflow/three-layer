package com.example.demo.api.rest;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.domain.UserService;
import lombok.AllArgsConstructor;

@AllArgsConstructor
@RestController
public class UserController {

    private final UserService userService;

    @PostMapping("/users")
    public UserWeb create(final UserWeb userWeb) {
        return UserWeb.toUserWeb(userService.createUser(userWeb.toUser()));
    }
}
