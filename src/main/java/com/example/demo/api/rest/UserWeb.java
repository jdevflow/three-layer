package com.example.demo.api.rest;

import com.example.demo.domain.User;
import lombok.Setter;

@Setter
public class UserWeb {

    private String id;
    private String login;
    private String password;

    public User toUser() {
        return User.builder()
                .password(password)
                .login(login)
                .build();
    }

    public static UserWeb toUserWeb(final User user) {
        var userWeb = new UserWeb();
        userWeb.setId(user.getId());
        userWeb.setLogin(user.getLogin());
        return userWeb;
    }
}
