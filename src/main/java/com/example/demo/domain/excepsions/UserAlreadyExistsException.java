package com.example.demo.domain.excepsions;

public class UserAlreadyExistsException extends RuntimeException {

    public UserAlreadyExistsException(final String email) {
        super(email);
    }
}
