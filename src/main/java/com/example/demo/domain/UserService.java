package com.example.demo.domain;

import java.util.UUID;
import javax.validation.Valid;

import org.springframework.stereotype.Service;

import com.example.demo.domain.excepsions.UserAlreadyExistsException;
import com.example.demo.infrastructure.UserRepository;
import lombok.AllArgsConstructor;


@AllArgsConstructor
@Service
public class UserService {

    private final UserRepository repository;

    public User createUser(@Valid final User user) {
        if (repository.findByLogin(user.getLogin()).isPresent()) {
            throw new UserAlreadyExistsException(user.getLogin());
        }
        var userToSave = User.builder()
                .id(UUID.randomUUID().toString())
                .login(user.getLogin())
                // тут через хэш или сервис использовать.
                .password(user.getPassword())
                .build();
        return repository.create(userToSave);
    }
}
