package com.example.demo.domain;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import lombok.Builder;
import lombok.Getter;

@Builder
@Getter
public class User {

    private static final int LOGIN_LENGTH_MIN = 4;

    private static final int LOGIN_LENGTH_MAX= 32;

    private String id;

    @Size(min = LOGIN_LENGTH_MIN,
          max = LOGIN_LENGTH_MAX,
          message = "Имя пользователя должно быть от " + LOGIN_LENGTH_MIN + " до " + LOGIN_LENGTH_MAX + " символов.")
    private String login;

    @NotBlank(message = "Пароль не может быть пустым")
    private String password;
}
